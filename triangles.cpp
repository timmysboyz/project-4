/*
 * CSc103 Project 4: Triangles
 * See readme.html for details.
 * Please list all references you made use of in order o complete the
 * assignment: your classmates, websites, etc.  Aside from the lecture notes
 * and the book, please list everything.  And remember- citing a source does
 * NOT mean it is okay to COPY THAT SOURCE.  What you submit here **MUST BE
 * YOUR OWN WORK**.
 * References:
 *
 *
 * Finally, please indicate approximately how many hours you spent on this:
 * #hours:
 */

#include "triangles.h" // import the prototypes for our triangle class.
#include <iostream>
using std::cout;
#include <vector>
using std::vector;
#include <algorithm>
using std::sort;

// note the "triangle::" part.  We need to specify the function's
// FULL name to avoid confusion.  Else, the compiler will think we
// are just defining a new function called "perimeter"
unsigned long triangle::perimeter() {
	return s1+s2+s3;
}

unsigned long triangle::area() {
	// TODO: write this function.
	// Note: why is it okay to return an integer here?  Recall that
	// all of our triangles have integer sides, and are right triangle
	s...
	unsigned long sides[3]={s1,s2,s3}
	unsigned long b= sides[0];
	unsigned long h=sides[1];
	return (b*h)/2;

	return 0;
}

void triangle::print() {
	cout << "[" << s1 << "," << s2 << "," << s3 << "]";
}

bool congruent(triangle t1, triangle t2) {
	// TODO: write this function.
	return false;
}

bool similar(triangle t1, triangle t2) {
	int a, b, c;
	a = t1.s1;
	b = t1.s2;
	c = t1.s3;
unsigned long tr1[3] = {a,b,c};
sort (tr1,tr1+3);

int d, e, f;
	d = t2.s1;
	e = t2.s2;
	f = t2.s3;
unsigned long tr2[3] = {d,e,f};
sort (tr2,tr2+3);

if (tr1[0]*tr2[1] == tr2[0]*tr1[1] && tr1[2]*tr2[1] == tr2[2]*tr1[1]) {return true;}
	return false;
}

vector<triangle> findRightTriangles(unsigned long l, unsigned long h) {
	// TODO: find all the right triangles with integer sides,
	// subject to the perimeter bigger than l and less than h
	vector<triangle> retval;
	triangle t1;
	t1.s1=l
	t1.s3=h;
	for (unsigned long i=0; i<t1.s3 ; p++) {
		t1.s2= i
		if (p>=t1.s1 && p<=t1.s3 (((t1.s1*t1.s1)+(i+1))==(t1.s1*t1s1))
			retval.push_back(t1);
		break
	}
}
	// storage for return value.
	return retval;
}

